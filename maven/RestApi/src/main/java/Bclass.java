import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class Bclass {

    /*@GET
    public Response szia(){
        System.out.println("szia Bclass");
        Cclass cclass=new Cclass();
        return Response.ok(cclass.szia()).build();
    }*/

    @GET
    public Response response(){
        return Response.ok("szia").build();
    }
    @GET
    @Path("/param")
    public Response doGet(@QueryParam("id") String id) {
        return Response.ok(" " + id).build();
    }

    @POST
    public Response doPost(String param2) {
        return Response.ok(param2).build();
    }
}
