import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;


@Path("/")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class Restful {

    @GET
    public javax.ws.rs.core.Response doGet(@QueryParam("id") String param1) {
        return javax.ws.rs.core.Response.ok(" " + param1).build();
    }

    @POST
    public javax.ws.rs.core.Response doPost(String param2) {
        return javax.ws.rs.core.Response.ok(param2).build();
    }
}

