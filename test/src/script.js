var app = angular.module('myApp', []);
app.controller('FetchController', ['$scope','$http',function($scope, $http) {
    $scope.data.singleSelect = '';
    $scope.data.hasParameters =false;
    $scope.data.response='';
    $scope.url="http://localhost:63342/test/src/index.html";

    $scope.data.hasParameters=function() {
        switch ($scope.data.hasParameters) {
            case "" :
                $scope.data.hasParameters = false; break;
            case "getNoParam" :
                $scope.data.hasParameters = false; break;
            case "getParam" :
                $scope.data.hasParameters = true; break;
            case "post" :
                $scope.data.hasParameters = true; break;
        }
    }

    $scope.data.response=function(){
        switch($scope.response){
            case "getNoParam" :
                $http({
                    method: 'GET',
                    url: $scope.url,})
                    .then(function successCallback(response) {
                        $scope.data.response = response.data;
                    }, function errorCallback(response){
                        console.log("Unable to perform get request");
                    });
                break;
            case "getParam":
                $http({
                    method:'GET',
                    url:$scope.url+'?'})
                    .then(function successCallback(response) {
                        $scope.data.response=response.data;
                    }, function errorCallback(response){
                        console.log("Unable to perform get request");
                    });
                break;
            case "post":
                $http({
                    method:'POST',
                    url:$scope.url+'?',
                    data:{'message' : message}})
                    .then(function successCallback(response) {
                        $scope.data.response=response.data;
                    }, function errorCallback(response){
                        console.log("POST-ing of data failed");
                    });
                break;
        }
    }

}]);